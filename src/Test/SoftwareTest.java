package Test;

import javax.swing.JFrame;

import GUI.SoftwareFrame;
import Model.MoneyCard;

public class SoftwareTest {
	private SoftwareFrame frame;
	private MoneyCard card;
	
	public static void main(String[] args) {
		new SoftwareTest();
	}
	public SoftwareTest() {
		// TODO Auto-generated constructor stub
		createFrame();
	}
	public void createFrame(){
		frame = new SoftwareFrame();
		frame.setVisible(true);
		frame.pack();
		frame.setSize(400, 400);
		TestCase();
		
	}
	
	public void TestCase(){
		card = new MoneyCard();
//		double cash = card.checkBalance();
		frame.setResults("balance : "+Double.toString(card.checkBalance()));
		frame.setResults("deposit : 100");
		card.deposit(100);
		frame.setResults("balance : "+Double.toString(card.checkBalance()));
		frame.setResults("withdraw : 50");
		card.withdraw(50);
		frame.setResults("balance : "+Double.toString(card.checkBalance()));
		
	}
}
