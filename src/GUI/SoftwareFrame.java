package GUI;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import Model.MoneyCard;

public class SoftwareFrame extends JFrame {

	private MoneyCard card;
	private JTextArea showResults;
	private String str = "Your Money";
	public SoftwareFrame() {
		// TODO Auto-generated constructor stub
		createPanal();
	}

	public void createPanal(){
		showResults = new JTextArea(str);
		add(showResults);
	}
	
	public void setResults(String s){
		str = str + "\n" + s;
		showResults.setText(str);
	}
}
